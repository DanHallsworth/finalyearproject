@apps =
    [
        {
            :application => 'outlook',
            :category => "emails",
            :file_artifact =>
                [
                    {
                        :filetypes => "deleted_emails",
                        :path => 'LocalAppData',
                        :dir => 'Identities',
                        :artifact => "C:/Users/daniel hallsworth/Desktop/yeet.txt",
                        :description => "Outlook's Deleted emails"},
                    :regexp_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :regexps =>[/password:.*/, /username:.*/],
                        },
                        {
                            :extraction_description => "known contacts in deleted emails",
                            :extraction_type => 'contacts',
                            :regexps => [/From:.*/, /To:.*/],
                        }],

                    :json_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :jsons => ['username', 'password']
                        }
                    ]
                ]
        }

    ]

file_from_artefacts = @apps[0][:file_artifact][0][:artifact]
known_contacts = @apps[0][:file_artifact][1][:regexp_searches][1][:regexps]

from_regex = known_contacts[0].source
to_regex = known_contacts[1].source


storing_from_array = []
storing_to_array = []

File.open("#{file_from_artefacts}").each do |contact_file|
  store_from = contact_file.match(from_regex)
  store_to   = contact_file.match(to_regex)

  if store_from || store_to
    storing_from_array << store_from
    storing_to_array << store_to
end
end

puts storing_to_array.flatten.uniq
puts storing_from_array.flatten.uniq