@apps =
    [
        {
            :application => 'outlook',
            :category => "emails",
            :file_artifact =>
                [
                    {
                        :filetypes => "deleted_emails",
                        :path => 'LocalAppData',
                        :dir => 'Identities',
                        :artifact => "C:/Users/daniel hallsworth/Desktop/yeet.txt",
                        :description => "Outlook's Deleted emails"},
                    :regexp_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :regexps =>[/password:.*/, /username:.*/],
                        },
                        {
                            :extraction_description => "known contacts in deleted emails",
                            :extraction_type => 'contacts',
                            :regexps => ['From:.*', 'To:.*'],
                        }],

                    :json_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :jsons => ['username', 'password']
                        }
                                      ]


        },
   

    {
        :filetypes => "draft_emails",
        :path => 'LocalAppData',
        :dir => 'Identities',
        :artifact => "Drafts.dbx",
        :description => "Outlook's unsent emails"},
    {
        :filetypes => "email_logs",
        :path => 'LocalAppData',
        :dir => 'Identities',
        :artifact => "Folders.dbx",
        :description => "Outlook's Folders"},
    {
        :filetypes => "received_emails",
        :path => 'LocalAppData',
        :dir => 'Identities',
        :artifact => "Inbox.dbx",
        :description => "Outlook's received emails"},
    {
        :filetypes => "email_logs",
        :path => 'LocalAppData',
        :dir => 'Identities',
        :artifact => "Offline.dbx",
        :description => "Outlook's offline emails"},
    {
        :filetypes => "email_logs",
        :path => 'LocalAppData',
        :dir => 'Identities',
        :artifact => "Outbox.dbx",
        :description => "Outlook's sent emails"},
    {
        :filetypes => "sent_emails",
        :path => 'LocalAppData',
        :dir => 'Identities',
        :artifact => "Sent Items.dbx",
        :description => "Outlook's sent emails"}

    ]
