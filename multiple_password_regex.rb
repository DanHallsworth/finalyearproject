@apps =
    [
        {
            :application => 'outlook',
            :category => "emails",
            :file_artifact =>
                [
                    {
                        :filetypes => "deleted_emails",
                        :path => 'LocalAppData',
                        :dir => 'Identities',
                        :artifact => "C:/Users/daniel hallsworth/Desktop/yeet.txt",
                        :description => "Outlook's Deleted emails"},
                    :regexp_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :regexps =>[/password:.*/, /username:.*/],
                        },
                        {
                            :extraction_description => "known contacts in deleted emails",
                            :extraction_type => 'contacts',
                            :regexps => [/From:.*/, /To:.*/],
                        }],

                    :json_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :jsons => ['username', 'password']
                        }
                    ]
                ]
        },{
            :application => 'outlook',
            :category => "emails",
            :file_artifact =>
                [
                    {
                        :filetypes => "deleted_emails",
                        :path => 'LocalAppData',
                        :dir => 'Identities',
                        :artifact => "C:/Users/daniel hallsworth/Desktop/yeet2.txt",
                        :description => "Outlook's Deleted emails"},
                    :regexp_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :regexps =>[/password:.*/, /username:.*/],
                        },
                        {
                            :extraction_description => "known contacts in deleted emails",
                            :extraction_type => 'contacts',
                            :regexps => [/From:.*/, /To:.*/],
                        }],

                    :json_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :jsons => ['username', 'password']
                        }
                    ]
                ]
        }

    ]
store_files_collected = []
store_file_info = ''
allowed_user_array = []
allowed_pass_array = []

@apps.collect {|gather_file_from_array| store_files_collected << gather_file_from_array[:file_artifact][0][:artifact] }

store_files_collected.collect {|open_collected_files| store_file_info << File.read("#{open_collected_files}") }

@apps.collect {|get_regex_user_pass|  allowed_pass_array << get_regex_user_pass[:file_artifact][1][:regexp_searches][0][:regexps]}

allowed_pass_string = ""

allowed_pass_string << allowed_pass_array[0][0].source

pass_into_regex = allowed_pass_string
match_regex_password = /#{pass_into_regex}/
puts store_file_info.scan(match_regex_password)
