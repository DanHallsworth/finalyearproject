@apps =
    [
        {
            :application => 'outlook',
            :category => "emails",
            :file_artifact =>
                [
                    {
                        :filetypes => "deleted_emails",
                        :path => 'LocalAppData',
                        :dir => 'Identities',
                        :artifact => "C:/Users/daniel hallsworth/Desktop/yeet.txt",
                        :description => "Outlook's Deleted emails"},
                    :regexp_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :regexps =>[/password:.*/, /username:.*/],
                        },
                        {
                            :extraction_description => "known contacts in deleted emails",
                            :extraction_type => 'contacts',
                            :regexps => [/From:.*/, /To:.*/],
                        }],

                    :json_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :jsons => ['username', 'password']
                        }
                    ]
                ]
        }, {
            :application => 'outlook',
            :category => "emails",
            :file_artifact =>
                [
                    {
                        :filetypes => "deleted_emails",
                        :path => 'LocalAppData',
                        :dir => 'Identities',
                        :artifact => "C:/Users/daniel hallsworth/Desktop/mmfm.txt",
                        :description => "Outlook's Deleted emails"},
                    :regexp_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :regexps =>[/password:.*/, /username:.*/],
                        },
                        {
                            :extraction_description => "known contacts in deleted emails",
                            :extraction_type => 'contacts',
                            :regexps => [/From:.*/, /To:.*/],
                        }],

                    :json_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :jsons => ['username', 'password']
                        }
                    ]
                ]
        }, {
            :application => 'outlook',
            :category => "emails",
            :file_artifact =>
                [
                    {
                        :filetypes => "deleted_emails",
                        :path => 'LocalAppData',
                        :dir => 'Identities',
                        :artifact => "C:/Users/daniel hallsworth/Desktop/note.txt",
                        :description => "Outlook's Deleted emails"},
                    :regexp_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :regexps =>[/password:.*/, /username:.*/],
                        },
                        {
                            :extraction_description => "known contacts in deleted emails",
                            :extraction_type => 'contacts',
                            :regexps => [/From:.*/, /To:.*/],
                        }],

                    :json_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :jsons => ['username', 'password']
                        }
                    ]
                ]
        }


    ]

file_array = []

@apps.any? do |x|
  file_array << x[:file_artifact][0][:artifact].to_s
  file_array.any? do |test|
    begin
      File.open("#{test}" ).any? do |open|
        puts open

      end
    rescue Errno::ENOENT => e
      $stderr.puts "Caught the exception: #{e}"
    end
  end
end








