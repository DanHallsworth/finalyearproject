@apps =
    [
        {
            :application => 'outlook',
            :category => "emails",
            :file_artifact =>
                [
                    {
                        :filetypes => "deleted_emails",
                        :path => 'LocalAppData',
                        :dir => 'Identities',
                        :artifact => "C:/Users/daniel hallsworth/Desktop/yeet.txt",
                        :description => "Outlook's Deleted emails"},
                    :regexp_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :regexps =>[/password:.*/, /username:.*/],
                        },
                        {
                            :extraction_description => "known contacts in deleted emails",
                            :extraction_type => 'contacts',
                            :regexps => ['From:.*', 'To:.*'],
                        }],

                    :json_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :jsons => ['username', 'password']
                        }
                    ]
                ]
        }

    ]

a = @apps[0][:file_artifact][0][:artifact]
c = @apps[0][:file_artifact][1][:regexp_searches][0][:regexps]
d = c[0].source
e = c[1].source

storing_user = []
storing_pass = []

File.open("#{a}").each do |x|
  store_user = x.match(d)
  store_pass = x.match(e)
  if store_user || store_pass
    storing_user << store_user
    storing_pass << store_pass
  end
end

puts storing_user.flatten.uniq
puts storing_pass.flatten.uniq
