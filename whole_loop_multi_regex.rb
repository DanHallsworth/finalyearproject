@apps =
    [
        {
            :application => 'outlook',
            :category => "emails",
            :file_artifact =>
                [
                    {
                        :filetypes => "deleted_emails",
                        :path => 'LocalAppData',
                        :dir => 'Identities',
                        :artifact => "C:/Users/daniel hallsworth/Desktop/yeet.txt",
                        :description => "Outlook's Deleted emails"},
                    :regexp_searches => [
                        {
                            :extraction_description => "usernames or passwords found in deleted emails",
                            :extraction_type => 'credentials',
                            :regexps =>[/password:.*/, /username:.*/],
                        },
                        {
                            :extraction_description => "known contacts in deleted emails",
                            :extraction_type => 'contacts',
                            :regexps => ['From:.*', 'To:.*'],
                        }],

                    :json_searches => [
                        {
                            :extraction_description => "stev",
                            :extraction_type => 'credentials',
                            :regexps => ['jsonPass', 'jsonUser']
                        }
                    ]

                ]


        }

    ]


store_files_collected = []
store_file_info = ''
allowed_credentials_array = []
regex_to_compare = []
final_produce = []


@apps.collect {|gather_file_from_array| store_files_collected << gather_file_from_array[:file_artifact][0][:artifact] }

store_files_collected.collect {|open_collected_files| store_file_info << File.read("#{open_collected_files}") }

@apps.collect {|get_regex_user_pass|  allowed_credentials_array << get_regex_user_pass[:file_artifact][1][:regexp_searches]}

allowed_credentials_array.flatten.map {|regex_search|  regex_to_compare << regex_search[:regexps]}


regex_to_compare.flatten.map {|multiple_regex_convert| final_produce  << store_file_info.scan(/#{multiple_regex_convert}/) }

puts final_produce