@apps.each do |x|
  if x.key?(:artifact && :x_pass && :x_user)
    a = "#{x[:artifact]}"
    doc = Nokogiri::XML(File.read("#{a}"))
    p = "#{x[:x_pass]}"
    b = "#{x[:x_user]}"
    name = doc.xpath("#{b}")
    pass = doc.xpath("#{p}")
    puts '------------------------------------'
    puts "New File:  #{x[:artifact]}"
    puts '------------------------------------'
    puts name
    puts pass
  else
    a = "#{x[:artifact]}"
    team = File.read("#{a}")
    b = team
    l = b.gsub(/<\/?[^>]+>/, '')
    s = l.strip #gets rid of huge gaps in between text
    # #s = l.gsub(/\s+/, "")   this gets rid of all space, dont want this
    puts '------------------------------------'
    puts "New File:  #{x[:artifact]}"
    puts '------------------------------------'
    puts s
  end
end